'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Cria o Schema das tarefas 
var TaskSchema = new Schema({
    name: {
        type: String,
        required: 'Kindly enter the name of the task'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
})

// Cria o modelo no banco (é o que eu acho, já que no código do site não tem comentário, aff)
module.exports = mongoose.model('Tasks', TaskSchema);